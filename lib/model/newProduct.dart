class ProductNew{
  List<ProductDetailnew> newproduct;
  ProductNew({this.newproduct});
  ProductNew.fromJson(Map<String,dynamic> json){
    if(json['newproduct'] !=null){
      newproduct=new List<ProductDetailnew>();
      json['newproduct'].forEach((v){
        newproduct.add(new ProductDetailnew.fromJson(v));
      });
    }
  }
  Map<String,dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.newproduct != null) {
      data['newproduct'] = this.newproduct.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
class ProductDetailnew{
  String name;
  String description;
  int quantity;
  int price;
  String unit;
  String weight;
  String photo;

  ProductDetailnew({this.name, this.price, this.quantity, this.unit, this.weight,
    this.description, this.photo});
  ProductDetailnew.fromJson(Map<String,dynamic> json){
    name = json['name'];
//    price = json["price"];
//    quantity = json['quantity'];
    weight =json['weight'];
    unit =json['unit'];
    description = json['description'];
    photo = json['photo'];

  }
  Map<String,dynamic> toJson(){
    final Map<String,dynamic> data =new Map<String,dynamic>();
    data['name'] =this.name;
//    data['price'] =this.price;
//    data['quantity'] =this.quantity;
    data['weight'] =this.weight;
    data['unit'] =this.unit;
    data['description'] =this.description;
    data['photo'] =this.photo;
    return data;
  }
}
