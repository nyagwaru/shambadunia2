class SliderResponse{
  List<SliderDetail> slider;
  SliderResponse({this.slider});
  SliderResponse.fromJson(Map<String,dynamic> json){
    if(json['slider'] !=null){
      slider=new List<SliderDetail>();
      json['slider'].forEach((v){
        slider.add(new SliderDetail.fromJson(v));
      });
    }
  }
  Map<String,dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.slider != null) {
      data['slider'] = this.slider.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
class SliderDetail{
  String second;
  String photo;

  SliderDetail({this.second, this.photo});
  SliderDetail.fromJson(Map<String,dynamic> json){
    second = json['second'];
    photo = json['photo'];

  }
  Map<String,dynamic> toJson(){
    final Map<String,dynamic> data =new Map<String,dynamic>();
    data['second'] =this.second;
    data['photo'] =this.photo;
    return data;
  }
}
