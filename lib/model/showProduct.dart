class ProductResponse{
  List<ProductDetail> product;
  ProductResponse({this.product});
  ProductResponse.fromJson(Map<String,dynamic> json){
    if(json['product'] !=null){
      product=new List<ProductDetail>();
      json['product'].forEach((v){
        product.add(new ProductDetail.fromJson(v));
      });
    }
  }
  Map<String,dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.product != null) {
      data['product'] = this.product.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
class ProductDetail{
  String name;
  String description;
  int quantity;
  int price;
  String unit;
  String weight;
  String photo;

  ProductDetail({this.name, this.price, this.quantity, this.unit, this.weight,
    this.description, this.photo});
  ProductDetail.fromJson(Map<String,dynamic> json){
    name = json['name'];
//    price = json["price"];
//    quantity = json['quantity'];
    weight =json['weight'];
    unit =json['unit'];
    description = json['description'];
    photo = json['photo'];

  }
  Map<String,dynamic> toJson(){
    final Map<String,dynamic> data =new Map<String,dynamic>();
    data['name'] =this.name;
//    data['price'] =this.price;
//    data['quantity'] =this.quantity;
    data['weight'] =this.weight;
    data['unit'] =this.unit;
    data['description'] =this.description;
    data['photo'] =this.photo;
    return data;
  }
}
