import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path/path.dart';
import 'package:shambadunia/Payment/Payment_Screen.dart';
import 'package:shambadunia/model/payment.dart';
class Checkout extends StatelessWidget {
  final PaymentDetail pay;
  Checkout({this.pay});
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    IconData _add_icon() {
      switch (Theme.of(context).platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.add;
        case TargetPlatform.iOS:
          return Icons.arrow_back_ios;
      }
      assert(false);
      return null;
    }
    IconData _sub_icon() {
      switch (Theme.of(context).platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.remove;
        case TargetPlatform.iOS:
          return Icons.arrow_back_ios;
      }
      assert(false);
      return null;
    }


    IconData _backIcon() {
      switch (Theme.of(context).platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.arrow_back;
        case TargetPlatform.iOS:
          return Icons.arrow_back_ios;
      }
      assert(false);
      return null;
    }
    final double height = MediaQuery.of(context).size.height;
    AppBar appBar = AppBar(
      leading: IconButton(
        icon: Icon(_backIcon()),
        alignment: Alignment.centerLeft,
        tooltip: 'Back',
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      title: Text("Checkout"),
      backgroundColor: Colors.white,
      actions: <Widget>[
        new Padding(
          padding: const EdgeInsets.all(10.0),
          child: new Container(
            height: 150.0,
            width: 30.0,
            child: new GestureDetector(
              onTap: () {
                /*Navigator.of(context).push(
                  new MaterialPageRoute(
                      builder:(BuildContext context) =>
                      new CartItemsScreen()
                  )
              );*/
              },
            ),
          ),
        )
      ],
    );
return new Scaffold(
  key: _scaffoldKey,
  appBar: appBar,
  body: SingleChildScrollView(
    child: new Column(
      children: <Widget>[
        _verticalDivider(),
        new Container(
          alignment: Alignment.topLeft,
          margin:
          EdgeInsets.only(left: 12.0, top: 5.0, right: 0.0, bottom: 5.0),
          child: Center(
            child: new Text(
              'DELIVERY ADDRESS',
              style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.bold,
                  fontSize: 18.0),
            ),
          ),
        ),
        new Container(
          alignment:Alignment.center,
          height: 140.0,
          width: 220.0,
          margin: EdgeInsets.all(7.0),
          child: Card(
            elevation: 3.0,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Column(
                  children: <Widget>[
                    new Container(
                      margin: EdgeInsets.only(
                          left: 12.0,
                          top: 5.0,
                          right: 0.0,
                          bottom: 5.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            'Name:Elison Nyagwaru',
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 15.0,
                              letterSpacing: 0.5,
                            ),
                          ),
                          _verticalDivider(),
                          new Text(
                            'Place:mbagala Temeke',
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: 15.0,
                                letterSpacing: 0.5),
                          ),
                          _verticalDivider(),
                          new Text(
                            'Region:Dar es salaam',
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: 15.0,
                                letterSpacing: 0.5),
                          ),
                          _verticalDivider(),
                          new Text(
                            'Building no:House no 1345',
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: 15.0,
                                letterSpacing: 0.5),
                          ),

                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        _verticalDivider(),
        new Container(
          alignment: Alignment.topLeft,
          margin:
          EdgeInsets.only(left: 12.0, top: 5.0, right: 0.0, bottom: 5.0),
          child: new Text(
            'Order Summary',
            style: TextStyle(
                color: Colors.black87,
                fontWeight: FontWeight.bold,
                fontSize: 18.0),
          ),
        ),
        Container(
            margin: EdgeInsets.only(
                left: 12.0, top: 5.0, right: 12.0, bottom: 5.0),
            height: 50.0,
            child: ListView.builder(
                itemCount:1,
                itemBuilder: (BuildContext cont, int ind) {
                  return SafeArea(
                      child: Column(
                        children: <Widget>[
                          Divider(height: 15.0),
                          Container(
                            padding: EdgeInsets.all(5.0),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                              children: <Widget>[

                                Text("machungwa",
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.black87,
                                        fontWeight: FontWeight.bold)),
                                Text("Qty:10",
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.black87,
                                        fontWeight: FontWeight.bold)),
                                Text("1500 tsh",
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.black87,
                                        fontWeight: FontWeight.bold)),
                              ],

                            ),
                          ),
                        ],
                      ));
                })),
        Container(
            alignment: Alignment.bottomLeft,
            height: 50.0,
            child: Card(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(icon: Icon(Icons.info), onPressed: null),
                  Text(
                    'Total :',
                    style: TextStyle(
                        fontSize: 17.0,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    ' 20,000 Tsh',
                    style: TextStyle(fontSize: 17.0, color: Colors.black54),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      alignment: Alignment.center,
                      child: OutlineButton(
                          borderSide:
                          BorderSide(color: Colors.red),
                          child: const Text('CONFIRM ORDER'),
                          textColor: Colors.red,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Patment()));
                          },
                          shape: new OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          )),
                    ),
                  ),
                ],
              ),
            )),
      ],
    ),
  ),
);
  }


  _verticalDivider() => Container(
    padding: EdgeInsets.all(2.0),
  );

  _verticalD() => Container(
    margin: EdgeInsets.only(left: 3.0, right: 0.0, top: 0.0, bottom: 0.0),
  );




}
