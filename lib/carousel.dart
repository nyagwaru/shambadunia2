import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:http/http.dart' as http;
import 'package:shambadunia/model/sliderPhoto.dart';
class carousel extends StatefulWidget {
  @override
  _carouselState createState() => _carouselState();
}

class _carouselState extends State<carousel> {
  int _current =0;
  String url = "https://shambadunia.com/ShambaDuniaApi/api/slider";
  SliderResponse detail;
  SliderDetail  details;
  _carouselState({this.details});
  _fetchSlider() async {
    var response = await http.get(url);
    var decodeJson = jsonDecode(response.body);
    print("response" + response.body);
    setState(() {
      detail = SliderResponse.fromJson(decodeJson);
    });
    print(detail);
  }

@override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fetchSlider();
  }


  @override
  Widget build(BuildContext context) {
return new Scaffold(
  appBar: new AppBar(
    title: new Text("Carousel"),
  ),
  body: Center(
    child: Column(
      children: <Widget>[
        Container(
              height: 188.0,
              margin: EdgeInsets.only(left: 5.0),
              child: new Swiper(
                  itemCount: 4,
                  itemBuilder: (BuildContext context,int index){
                    return new Image.network("http://dev.shambadunia.com/images/slider/"+details.photo,
                      fit: BoxFit.fill,
                    );
                  },
                pagination: new SwiperPagination(),
                control: new SwiperControl(),
                autoplay: true,
              ),
            ),

      ],
    ),
  ),
);
  }
}
