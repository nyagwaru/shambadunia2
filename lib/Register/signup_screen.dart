import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shambadunia/HomeScreen.dart';
import 'package:shambadunia/Register/logind_signup.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
class Signup_Screen extends StatefulWidget {
  final Key fieldKey;
  final String hintText;
  final String labelText;
  final String helperText;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;
  const Signup_Screen({Key key, this.fieldKey, this.hintText, this.labelText, this.helperText, this.onSaved, this.validator, this.onFieldSubmitted}) : super(key: key);

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
      hintColor: Colors.red,
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: TextStyle(
            color: Colors.yellow,
            fontSize: 24.0
        ),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() => signup();
}
class signup extends State<Signup_Screen> {
  Response response;
  Dio dio=new Dio();
var _loading = false;
var _token ='';

  ShapeBorder shape;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();
  String dropdownGender ;
  String dropdownType ;
TextEditingController firstname=new TextEditingController();
TextEditingController lastname=new TextEditingController();
TextEditingController email=new TextEditingController();
TextEditingController phone=new TextEditingController();
TextEditingController address=new TextEditingController();
TextEditingController country=new TextEditingController();
TextEditingController password=new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;
  bool _formWasEdited = false;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    bool _obscureText = true;
    return new Scaffold(
        key: scaffoldKey,
        appBar: new AppBar(
          title: Text('Signup'),
          backgroundColor: Colors.greenAccent,
        ),
        body: SafeArea(
            child: new SingleChildScrollView(
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new SafeArea(
                      top: false,
                      bottom: false,
                      child: Card(
                          elevation: 5.0,
                          child: Form(
                              key: formKey,
                              autovalidate: _autovalidate,
                              child: SingleChildScrollView(
                                padding: const EdgeInsets.all(16.0),
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    children: <Widget>[

                                      const SizedBox(height: 24.0),
                                      TextFormField(
                                        controller:firstname ,
                                        decoration: const InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                            ),
                                            focusedBorder:  OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.purple,style: BorderStyle.solid),
                                            ),
                                            border: OutlineInputBorder(
                                              borderSide: const BorderSide(),
//                                            borderRadius: BorderRadius.circular(32),
                                            ),
                                            icon: Icon(Icons.person,color: Colors.black,),
                                            hintText: 'Enter first name',
                                            labelText: 'First Name',
                                            labelStyle: TextStyle(color: Colors.black54)
                                        ),
                                        keyboardType: TextInputType.text,
                                        validator: (val) =>
                                        val.length < 1 ? 'Enter first name' : null,
//                                        onSaved: (val) => _firstname = val,
                                      ),
                                      const SizedBox(height: 24.0),
                                      TextFormField(
                                        controller:lastname ,
                                        decoration: const InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                            ),
                                            focusedBorder:  OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.purple,style: BorderStyle.solid),
                                            ),
                                            border: OutlineInputBorder(
                                              borderSide: const BorderSide(),
//                                            borderRadius: BorderRadius.circular(32),
                                            ),
                                            icon: Icon(Icons.perm_identity,color: Colors.black,),
                                            hintText: 'Enter last name',
                                            labelText: 'Last Name',
                                            labelStyle: TextStyle(color: Colors.black54)
                                        ),
                                        keyboardType: TextInputType.text,
                                        validator: (val) =>
                                        val.length < 1 ? 'Enter last name' : null,
//                                        onSaved: (val) => _lastname = val,
                                      ),
                                      const SizedBox(height: 24.0),
                                      TextFormField(
                                        controller:email ,
                                        decoration: const InputDecoration(
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                          ),
                                          focusedBorder:  OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.purple,style: BorderStyle.solid),
                                          ),
                                          border: OutlineInputBorder(
                                            borderSide: const BorderSide(),
//                                            borderRadius: BorderRadius.circular(32),
                                          ),
                                            icon: Icon(Icons.email,color: Colors.black,),
                                            hintText: 'Your email address',
                                            labelText: 'E-mail',
                                            labelStyle: TextStyle(color: Colors.black54)
                                        ),
                                        keyboardType: TextInputType.emailAddress,
                                        validator: validateEmail,
                                      ),

                                      const SizedBox(height: 24.0),
                                      TextFormField(
                                        controller: phone,
                                        decoration: const InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                            ),
                                            focusedBorder:  OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.purple,style: BorderStyle.solid),
                                            ),
                                            border: OutlineInputBorder(
                                              borderSide: const BorderSide(),
//                                            borderRadius: BorderRadius.circular(32),
                                            ),
                                            icon: Icon(Icons.phone_android,color: Colors.black,),
                                            hintText: 'Your phone number',
                                            labelText: 'Phone',
                                            labelStyle: TextStyle(color: Colors.black54)
                                        ),
                                        keyboardType: TextInputType.phone,
                                        validator: validateMobile,
                                      ),
                                      const SizedBox(height: 24.0),
                                      TextFormField(
                                        controller: address,
                                        decoration: const InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                            ),
                                            focusedBorder:  OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.purple,style: BorderStyle.solid),
                                            ),
                                            border: OutlineInputBorder(
                                              borderSide: const BorderSide(),
//                                            borderRadius: BorderRadius.circular(32),
                                            ),
                                            icon: Icon(Icons.add_location,color: Colors.black,),
                                            hintText: 'Enter  Address',
                                            labelText: 'Address',
                                            labelStyle: TextStyle(color: Colors.black54)
                                        ),
                                        keyboardType: TextInputType.text,
                                        validator: (val) =>
                                        val.length < 1 ? 'Enter Address' : null,
//                                        onSaved: (val) => _address = val,
                                      ),
                                      const SizedBox(height: 24.0),
                                      TextFormField(
                                        controller: country,
                                        decoration: const InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                            ),
                                            focusedBorder:  OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.purple,style: BorderStyle.solid),
                                            ),
                                            border: OutlineInputBorder(
                                              borderSide: const BorderSide(),
//                                            borderRadius: BorderRadius.circular(32),
                                            ),
                                            icon: Icon(Icons.flag,color: Colors.black,),
                                            hintText: 'Enter  Country',
                                            labelText: 'Country',
                                            labelStyle: TextStyle(color: Colors.black54)
                                        ),
                                        keyboardType: TextInputType.text,
                                        validator: (val) =>
                                        val.length < 1 ? 'Enter Country' : null,
//                                        onSaved: (val) => _country = val,
                                      ),
                                      const SizedBox(height: 24.0),
                                      Center (
                                        child: Row(
                                          children: <Widget>[
                                            Icon(FontAwesomeIcons.venusMars),
                                            _space(),
                                            Center(
                                              child: DropdownButton<String>(
                                                hint: new Text('Select Gender',style: TextStyle(fontSize: 18.0),),
                                                value: dropdownGender,
                                                onChanged: (String newValue){
                                                  setState(() {
                                                    dropdownGender = newValue;
                                                  });
                                                },
                                                items: <String>['Male','Female'].map<DropdownMenuItem<String>>((String value1){
                                                  return DropdownMenuItem<String>(
                                                    value: value1,
                                                    child: Text(value1),
                                                  );
                                                }).toList(),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(height: 24.0),
                                      Center (
                                        child: Row(
                                          children: <Widget>[
                                            Icon(FontAwesomeIcons.sortDown),
                                            _space(),
                                            Center(
                                              child: DropdownButton<String>(
                                                hint: new Text('Select Account Type',style: TextStyle(fontSize: 18.0),),
                                                value: dropdownType,
                                                onChanged: (String newValue){
                                                  setState(() {
                                                    dropdownType = newValue;
                                                  });
                                                },
                                                items: <String>['Seller','Buyer'].map<DropdownMenuItem<String>>((String value1){
                                                  return DropdownMenuItem<String>(
                                                    value: value1,
                                                    child: Text(value1),
                                                  );
                                                }).toList(),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(height: 24.0),
                                      TextFormField(
                                        controller: password,
                                        obscureText: true,
                                        decoration: const InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                            ),
                                            focusedBorder:  OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.purple,style: BorderStyle.solid),
                                            ),
                                            border: OutlineInputBorder(
                                              borderSide: const BorderSide(),
//                                            borderRadius: BorderRadius.circular(32),
                                            ),
                                            icon: Icon(Icons.lock,color: Colors.black,),
                                            hintText: 'Your password',
                                            labelText: 'Password',
                                            labelStyle: TextStyle(color: Colors.black54)
                                        ),

                                        validator: (val) =>
                                        val.length < 6 ? 'Password too short.' : null,
//                                        onSaved: (val) => _password = val,
                                      ),
                                      SizedBox(height: 35.0,),
                                      new Container(
                                        child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            new Container(
                                              alignment: Alignment.center,
                                              child: new RaisedButton(
                                                onPressed:this.submit,
                                                textColor: Colors.white,
                                                color:Colors.blue,
                                                splashColor: Colors.redAccent,
                                                padding:const EdgeInsets.all(8.0),
                                                child: new Text(
                                                    "Signup"
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      new SizedBox(
                                          height: 15.0
                                      ),
                                      new Container(
                                        child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            new Container(
                                              alignment: Alignment.bottomRight,
                                              child: new GestureDetector(
                                                onTap: (){
                                                  Navigator.push(context, MaterialPageRoute(
                                                      builder: (context) => Login_Screen()));
                                                },
                                                child: Text('Already Login? click here',style: TextStyle(
                                                    color: Colors.black,fontSize: 17.0,fontWeight: FontWeight.bold
                                                ),),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ]
                                ),
                              )
                          )        //login,
                      ))
                ],
              ),
            )
        ));
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  String validateMobile(String value) {
// Indian Mobile number are of 10 digit only
    if (value.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }
  register() async{

      FormData formData=new FormData.from(
          {
            'lastname':lastname.text,
            'firstname':firstname.text,
            'email':email.text,
            'phone':phone.text,
            'address':address.text,
            'country':country.text,
            'gender':dropdownGender,
            'type':dropdownType,
            'password':password.text
          }
      );
      response = await dio.post("https://shambadunia.com/ShambaDuniaApi/api/UserReg",
          data: formData,
          options: Options(
            method: 'POST',
            responseType: ResponseType.json
          ));
      if(response.statusCode == 200){
        print("success");
        print(response.statusCode);
        print(response.data);
        print(response.headers);
        Route route = MaterialPageRoute(builder: (context) =>Login_Screen());
        Navigator.pushReplacement(context, route);
      }else{
        print("unauthorized");
      }
  }
  Future submit() async{
    final form = formKey.currentState;
    if (form.validate()) {
      setState(() {
        _loading =true;
      });
      _loading ?showDialog<void>(
        context: context,
        builder: (BuildContext context){
          return SpinKitWave(
            color: Colors.green,
            size: 40.0,
          );
        },
      ):null;
      register();
    }

  }
  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(value)
    ));
  }
  _verticalD() => Container(
    margin: EdgeInsets.only(left: 10.0, right: 0.0, top: 0.0, bottom: 0.0),
  );
  _space() => Container(
    margin: EdgeInsets.only(left: 18.0, right: 0.0, top: 0.0, bottom: 0.0),
  );
}
