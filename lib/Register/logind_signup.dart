import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shambadunia/HomeScreen.dart';
import 'package:shambadunia/Register/signup_screen.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
class Login_Screen extends StatefulWidget {
  final Key fieldKey;
  final String hintText;
  final String labelText;
  final String helperText;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;

  const Login_Screen({Key key, this.fieldKey, this.hintText, this.labelText, this.helperText, this.onSaved, this.validator, this.onFieldSubmitted}) : super(key: key);
  @override
  State<StatefulWidget> createState() => login();
}

class login extends State<Login_Screen> {
var _loading =false;
Response response;
Dio dio=new Dio();
//  ShapeBorder shape;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();

  String _email;
  String _password;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController email=new TextEditingController();
  TextEditingController password=new TextEditingController();
  bool _autovalidate = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        key: scaffoldKey,
        body: SafeArea(
          child: new SingleChildScrollView(
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new SafeArea(
                    top: false,
                    bottom: false,
                    child: Card(
                        elevation: 5.0,
                        child: Form(
                            key: formKey,
                            autovalidate: _autovalidate,
                            child: SingleChildScrollView(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: <Widget>[
                                    const SizedBox(height: 24.0),
                                    Center(
                                      child: Text('Login',style:new TextStyle(fontSize:30.0,color: Colors.black,fontWeight: FontWeight.bold)),
                                    ),
                                    Card(
                                      color: Colors.blueGrey,
                                      semanticContainer: true,
                                      clipBehavior: Clip.antiAliasWithSaveLayer,
                                      child: Icon(Icons.person,size: 150.0,),
                                      shape:CircleBorder(

                                      ),
                                      elevation: 5,
                                      margin: EdgeInsets.all(10.0),
                                    ),
                                    TextFormField(
                                      controller: email,
                                      decoration: const InputDecoration(
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                          ),
                                          focusedBorder:  OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.purple,style: BorderStyle.solid),
                                          ),
                                          border: OutlineInputBorder(
                                            borderSide: const BorderSide(),
                                          ),
                                          icon: Icon(Icons.email,color: Colors.black38,),
                                          hintText: 'Your email address',
                                          labelText: 'E-mail',
                                          labelStyle: TextStyle(color: Colors.black54)
                                      ),
                                      keyboardType: TextInputType.emailAddress,
                                      validator: (val) =>
                                      !val.contains('@') ? 'Not a valid email.' : null,
//                                      onSaved: (val) => _email = val,
                                    ),
                                    const SizedBox(height: 24.0),
                                    TextFormField(
                                      controller: password,
                                      obscureText: true,
                                      decoration: const InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                          ),
                                          focusedBorder:  OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.purple,style: BorderStyle.solid),
                                          ),
                                          border: OutlineInputBorder(
                                            borderSide: const BorderSide(),
//                                            borderRadius: BorderRadius.circular(32),
                                          ),
                                          icon: Icon(Icons.lock,color: Colors.black38,),
                                          hintText: 'Your password',
                                          labelText: 'Password',
                                          labelStyle: TextStyle(color: Colors.black54)
                                      ),
                                      validator: (val) =>
                                      val.length < 6 ? 'Password too short.' : null,
                                      onSaved: (val) => _password = val,
                                    ),
                                    SizedBox(height: 35.0,),
//                                    new Container(
////                                      alignment: Alignment.center,
////                                      child: Row(
////                                        mainAxisSize: MainAxisSize.max,
////                                        children: <Widget>[
//////                                          new RaisedButton(
//////                                           onPressed:this.submit,
//////                                            textColor: Colors.red,
//////                                            color:Colors.green,
//////                                            padding:const EdgeInsets.all(8.0),
//////                                            child: new Text(
//////                                              "LOGIN"
//////                                            ),
//////                                          ),
////                                        ],
////                                      ),
////                                    ),
                                    new Container(
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Container(
                                            alignment: Alignment.center,
                                            child:  new RaisedButton(
                                           onPressed:(){
                                             submit();
                                           },
                                            textColor: Colors.white,
                                            color:Colors.blue,
                                            splashColor: Colors.redAccent,
                                            padding:const EdgeInsets.all(8.0),
                                            child: new Text(
                                              "LOGIN"
                                            ),
                                          ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    new SizedBox(
                                        height: 20.0
                                    ),
                                    new Container(
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          new Container(
                                            alignment: Alignment.bottomLeft,
                                            margin: EdgeInsets.only(left: 10.0),
                                            child: new GestureDetector(
                                              onTap: (){
                                              },
                                              child: Text('FORGOT PASSWORD?',style: TextStyle(
                                                  color: Colors.black,fontSize: 13.0
                                              ),),
                                            ),
                                          ),
                                          new Container(
                                            alignment: Alignment.bottomRight,
                                            child: new GestureDetector(
                                              onTap: (){
                                                Navigator.push(context, MaterialPageRoute(
                                builder: (context) => Signup_Screen()));
                                              },
                                              child: Text('SIGNUP HERE',style: TextStyle(
                                                  color: Colors.black,fontSize: 13.0,fontWeight: FontWeight.bold
                                              ),),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),

                                  ]
                              ),
                            )

                        )        //login,
                    ))
              ],
            ),
          )
        ));
  }
 logins() async{
    try{
      FormData formData=new FormData.from(
          {
            'email':email.text,
            'password':password.text
          }
      );
      response = await dio.post("https://shambadunia.com/ShambaDuniaApi/api/login",
          data: formData,
          options: Options(
            method: 'POST',
            responseType: ResponseType.json,
          ));
      if(response.statusCode == 200){
        print("success");
        print(response.statusCode);
        print(response.headers);
        Route route = MaterialPageRoute(builder: (context) =>Home_screen());
        Navigator.pushReplacement(context, route);
      }else{
        print("unauthorized");
      }
    }
    catch(e){
      print('failed');
    }

  }
  Future submit() async{
    final form = formKey.currentState;
    if (form.validate()) {
      setState(() {
        _loading =true;
      });
//      form.save();
//      _spinkit();
//      _performLogin();
    _loading ?showDialog<void>(
    context: context,
    builder: (BuildContext context){
      return SpinKitWave(
      color: Colors.green,
      size: 40.0,
      );
    },
    ):null;
    logins();
      }
    else{
      showInSnackBar('Please enter Valid credentials');
    }
    }

//  }
//  void _spinkit(){
//    SpinKitFadingCircle(
//      color: Colors.white,
//      size: 50.0,
//      controller: AnimationController(duration: const Duration(milliseconds: 1200), vsync: this),
//    );
//  }
  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(value)
    ));
  }
  void _performLogin() {
    // This is just a demo, so no actual login here.
    Navigator.push(context, MaterialPageRoute(builder: (context)=> Home_screen()));
  }


  }
