import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shambadunia/Payment/checkout_screen.dart';
import 'package:shambadunia/ProductPAge/detailProduct.dart';
import 'package:shambadunia/model/payment.dart';
import 'package:shambadunia/model/showProduct.dart';
class ProductShow extends StatelessWidget {
  final ProductDetail detail;
  final PaymentDetail pay;
  ProductShow({this.detail,this.pay,this.shape});
  final ShapeBorder shape;
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      elevation: 4.0,
      margin: EdgeInsets.all(4.0),
      child: InkWell(
        radius: 4.0,
        child: getScaffold(context),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) =>
                detailProduct(
                  detail: detail,
                ),
            ),
          );
        },
      ),
    );
  }

  getScaffold(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final TextStyle titleStyle =
    theme.textTheme.headline.copyWith(color: Colors.white);
    final TextStyle descriptionStyle = theme.textTheme.subhead;
    return SafeArea(
      top: false,
      bottom: false,
      child: Container(
        padding: const EdgeInsets.all(2.0),
        height: 600.0,
child: Card(
  shape: shape,
  child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      SizedBox(
        height: 150.0,
        child: Stack(
          children: <Widget>[
            Hero(
              tag: detail,
              child: Image.network(
                  "http://dev.shambadunia.com/productsimages/l/"+detail.photo,
                fit: BoxFit.cover,
                height: double.infinity,
                width: double.infinity,
              ),
            )
          ],
        ),
      ),
      Divider(),
    Expanded(
      child: Container(
        padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
        child: DefaultTextStyle(
          style: descriptionStyle,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Text(
                  detail.name,
                  style: descriptionStyle.copyWith(
                    color: Colors.black87
                  ),
                ),

              )
            ],
          ),
        ),
      ),
    ),
//      Hero(
//        tag: pay,
         Container(
            alignment: Alignment.center,
            child: OutlineButton(
              borderSide: BorderSide(color: Colors.amber.shade500),
              child: const Text('Buy'),
              textColor: Colors.amber.shade500,
              onPressed: (){
                Navigator.push(context,MaterialPageRoute(builder: (context)=>Checkout()));
              },
              shape: new OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30.0)
              ),
            ),
          )
//      )
    ],
  ),
),
      ),
    );
  }
}


