import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shambadunia/Cart_Screen.dart';
import 'package:shambadunia/NewProducts/NewProductShow.dart';
import 'package:shambadunia/model/newProduct.dart';
class newProduct extends StatefulWidget {
  @override
  _newProductState createState() => _newProductState();
}

class _newProductState extends State<newProduct> {
  String url = "https://shambadunia.com/ShambaDuniaApi/api/newProduct";
  ProductNew detail;
  List list = ['12', '11'];
  fetchProduct() async {
    var response = await http.get(url);
    var decodeJson = jsonDecode(response.body);
    print("response" + response.body);
    setState(() {
      detail = ProductNew.fromJson(decodeJson);
    });
    print(detail);
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchProduct();
  }
  @override
  Widget build(BuildContext context) {
    IconData _backIcon() {
      switch (Theme.of(context).platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.arrow_back;
        case TargetPlatform.iOS:
          return Icons.arrow_back_ios;
      }
      assert(false);
      return null;
    }
    var size = MediaQuery.of(context).size;
    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = size.width / 2;
    final Orientation orientation = MediaQuery.of(context).orientation;
    return new Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(_backIcon()),
            alignment: Alignment.centerLeft,
            tooltip: 'Back',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text("New Product"),
          backgroundColor: Colors.white,
          actions: <Widget>[
            IconButton(
              tooltip: 'Search',
              icon: const Icon(Icons.search),
              onPressed: () async {
                final int selected = await showSearch<int>(
                  context: context,
                  //delegate: _delegate,
                );

              },
            ),
            new Padding(
              padding: const EdgeInsets.all(10.0),
              child: new Container(
                height: 150.0,
                width: 30.0,
                child: new GestureDetector(
                  onTap: () {
                    /*Navigator.of(context).push(
                  new MaterialPageRoute(
                      builder:(BuildContext context) =>
                      new CartItemsScreen()
                  )
              );*/
                  },
                  child: Stack(
                    children: <Widget>[
                      new IconButton(
                          icon: new Icon(
                            Icons.favorite,
                            color: Colors.black,
                          ),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> Cart_screen()));
                          }),
                      list.length == 0
                          ? new Container()
                          : new Positioned(
                          child: new Stack(
                            children: <Widget>[
                              new Icon(Icons.brightness_1,
                                  size: 20.0, color: Colors.orange.shade500),
                              new Positioned(
                                  top: 4.0,
                                  right: 5.5,
                                  child: new Center(
                                    child: new Text(
                                      list.length.toString(),
                                      style: new TextStyle(
                                          color: Colors.white,
                                          fontSize: 11.0,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  )),
                            ],
                          )),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
        body: detail == null ? Center(
          child: new SpinKitCircle(
            color: Colors.green,
            size: 40.0,
          ),
        ):Column(
          children: <Widget>[
            Container(
              child: Expanded(
                child: GridView.builder(
                  controller: new ScrollController(keepScrollOffset: false),
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  padding: const EdgeInsets.all(4.0),
                  itemCount:detail.newproduct.length ,
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                      childAspectRatio:(itemWidth / itemHeight),
                      crossAxisCount: 2),
                  itemBuilder: (BuildContext context,int index){
                    return NewProductShow(
                        detail:detail.newproduct[index]);
                  },
                ),
              ),
            )
          ],
        )
//      :StaggeredGridView.countBuilder(
//          crossAxisCount: 4,
//          itemCount:detail.product.length ,
//          itemBuilder: (BuildContext context,int index){
//            return ProductShow(
//              detail:detail.product[index]
//            );
//          },
//          staggeredTileBuilder: (_)=>StaggeredTile.fit(2),
//         mainAxisSpacing: 4.0,
//         crossAxisSpacing: 4.0,
//      )
    );
  }
}
